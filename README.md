On Target Cleaning is your choice for both residential and commercial carpet, floor, textile cleaning and floor repair in Bowling Green, KY. With years of experience in the floor care industry, we specialize in providing environmentally friendly, low moisture carpet cleaning.

Address: 308 Maple Ridge Street, Bowling Green, KY 42101

Phone: 270-693-3600
